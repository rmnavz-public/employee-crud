# Getting Started

1. Change the connectionstring at appsettings.json from cmp.EmployeeCrud.WebApp directory.

2. Run `docker-compose up --build` from root directory, to pull and build docker containers

3. Go to `localhost:5000` to access the web app.
