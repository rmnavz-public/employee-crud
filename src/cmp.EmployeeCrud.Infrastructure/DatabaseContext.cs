using cmp.EmployeeCrud.Domain;
using Microsoft.EntityFrameworkCore;

namespace cmp.EmployeeCrud.Infrastructure
{
    public class DatabaseContext : DbContext
    {

        public DatabaseContext(DbContextOptions options) : base(options)
        {

        }

        public DbSet<Employee> Employees { get; set; }
    }
}