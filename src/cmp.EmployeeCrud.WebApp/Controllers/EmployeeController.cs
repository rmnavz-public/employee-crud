using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using cmp.EmployeeCrud.Domain;
using cmp.EmployeeCrud.Infrastructure;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace cmp.EmployeeCrud.WebApp.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class EmployeeController : ControllerBase
    {
        private readonly ILogger<EmployeeController> _logger;
        private readonly DatabaseContext _databaseContext;

        public EmployeeController(ILogger<EmployeeController> logger, DatabaseContext databaseContext)
        {
            _logger = logger;
            _databaseContext = databaseContext;
        }

        [HttpGet]
        public async Task<IEnumerable<Employee>> Get()
        {
            return await _databaseContext.Employees.OrderByDescending(x => x.Id).ToListAsync();
        }

        [HttpGet("{id}")]
        public async Task<Employee> Get(int id)
        {
            return await _databaseContext.Employees.FirstOrDefaultAsync(x => x.Id == id);
        }

        [HttpPost]
        public async Task<Employee> Post([FromBody] Employee employee)
        {
            _databaseContext.Employees.Add(employee);
            await _databaseContext.SaveChangesAsync();
            return employee;
        }

        [HttpPut("{id}")]
        public async Task<Employee> Put(int id, [FromBody] Employee employee)
        {
            var model = await _databaseContext.Employees.FirstOrDefaultAsync(x => x.Id == id);
            if(model != null)
            {
                model.Name = employee.Name;
                model.Address = employee.Address;
                model.PhoneNumber = employee.PhoneNumber;
                await _databaseContext.SaveChangesAsync();
                return model;
            }
            return null;
        }

        [HttpDelete("{id}")]
        public async Task Delete(int id)
        {
            var model = await _databaseContext.Employees.FirstOrDefaultAsync(x => x.Id == id);
            if (model != null)
            {
                _databaseContext.Employees.Remove(model);
                await _databaseContext.SaveChangesAsync();
            }

        }
    }
}