import { Component, OnInit, Inject } from '@angular/core';
import { NgForm } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { Employee } from '../data/employee';
import { Router } from '@angular/router';

@Component({
  selector: 'app-employee-add',
  templateUrl: './employee-add.component.html',
  styleUrls: ['./employee-add.component.css']
})
export class EmployeeAddComponent implements OnInit {

  // constructor() { }

  private _http: HttpClient;
  private _baseUrl: string;
  private _router: Router;

  constructor(http: HttpClient, @Inject('BASE_URL') baseUrl: string, router: Router) {
    this._http = http;
    this._baseUrl = baseUrl;
    this._router = router;
  }

  ngOnInit() {
  }

  onSubmit(form: NgForm) {
    console.log('Your form data : ', form.value);
    this._http.post(this._baseUrl + 'employee', form.value).subscribe(result => {
      this._router.navigate(['/employees']);
    })
  }

}
