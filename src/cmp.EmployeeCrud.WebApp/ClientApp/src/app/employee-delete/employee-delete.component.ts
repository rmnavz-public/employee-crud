import { HttpClient } from '@angular/common/http';
import { Component, Inject } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Employee } from '../data/employee';

@Component({
  selector: 'app-employee-delete',
  templateUrl: './employee-delete.component.html',
  styleUrls: ['./employee-delete.component.css']
})
export class EmployeeDeleteComponent {

  constructor(activatedroute: ActivatedRoute, http: HttpClient, @Inject('BASE_URL') baseUrl: string, router: Router)
  {
    http.delete<Employee>(baseUrl + 'employee/' + activatedroute.snapshot.paramMap.get("id")).subscribe(result => {
      router.navigate(['/employees']);
    }, error => console.error(error));
  }

}
