import { HttpClient } from '@angular/common/http';
import { Component, Inject } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Employee } from '../data/employee';

@Component({
  selector: 'app-employee-edit',
  templateUrl: './employee-edit.component.html',
  styleUrls: ['./employee-edit.component.css']
})
export class EmployeeEditComponent {
  public employee: Employee;

  private _http: HttpClient;
  private _baseUrl: string;
  private _router: Router;
  private _activatedroute: ActivatedRoute;

  constructor(activatedroute: ActivatedRoute, http: HttpClient, @Inject('BASE_URL') baseUrl: string, router: Router)
  {
    this._http = http;
    this._baseUrl = baseUrl;
    this._router = router;
    this._activatedroute = activatedroute;
    http.get<Employee>(baseUrl + 'employee/' + activatedroute.snapshot.paramMap.get("id")).subscribe(result => {
      this.employee = result;
    }, error => console.error(error));
  }

  onSubmit(form: NgForm) {
    this._http.put(this._baseUrl + 'employee/' + this._activatedroute.snapshot.paramMap.get("id"), form.value).subscribe(result => {
      console.log(result);
      this._router.navigate(['/employees']);
    })
  }

}
