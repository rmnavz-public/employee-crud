namespace cmp.EmployeeCrud.Domain
{
    public abstract class BaseEntity
    {
        public int Id { get; set; }
    }
}