namespace cmp.EmployeeCrud.Domain
{
    public class Employee : BaseEntity
    {
        public string Name { get; set; }
        public string Address { get; set; }
        public string PhoneNumber { get; set; }
    }
}